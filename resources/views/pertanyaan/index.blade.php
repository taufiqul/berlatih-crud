@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3">
    <div class="card">
    <div class="card-header">
        <h3 class="card-tittle">Pertanyaan</h3>
</div>
                <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session ('success')}}
                    </div>
                @endif
                    <a class="btn btn-primary mb-2" href="pertanyaan/create">Create New Pertanyaan</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Pertanyaan</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  @forelse($pertanyaan as $key => $post)
                    <tr>
                        <td> {{ $key +1 }}</td>
                        <td> {{$post->pertanyaan}} </td>
                        <td> Actions </td>
                        <td style="display:flex;">
                            <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-info btn-sm">edit</a>
                            <form action="/pertanyaan/{{$post->id}}" method="post">
                            @csrf
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">Tidak ada pertanyaan</td>
                    </tr>
                  @endforelse
                    
                  </tbody>
                </table>
              </div>
    </div>
@endsection