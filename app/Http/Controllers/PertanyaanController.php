<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'pertanyaan' => 'required|unique:pertanyaan'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "pertanyaan" => $request["pertanyaan"]
        ]);

        return redicrect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dibuat!');

    }

    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id) {
        $post = DB::table('pertanyaan')->where('id' $id)->first();
        //dd($post);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id) {
        $post = DB::table('pertanyaan')->where('id' $id)->first();
        //dd($post);
        return view('pertanyaan.edit', compact('post'));


    public function update($id Request $request) {
        $request->validate([
            'pertanyaan' => 'required|unique:pertanyaan'
        ]);

        $query = DB::table('pertanyaan')
                        ->where('id', $id)
                        ->update([
                            'pertanyaan' => $request['pertanyaan']
                        ]);

        return redicrect('/pertanyaan')->with('success', 'Update Pertanyaan Berhasil!');

    }

    public function destroy($id) {
        $query = DB::table('pertanyaan')->where('id' $id)->delete();
        return redicrect('/pertanyaan')->with('success', 'Delete Pertanyaan Berhasil!');
    }

}
