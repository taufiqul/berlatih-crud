<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/', function () {
    return view('items.create');
});

Route::get('/data-table', function () {
    return view('items.index');
});

Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@show');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::deelte('/pertanyaan/{id}', 'PertanyaanController@destroy');